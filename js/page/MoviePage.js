/**
 * 
 * MoviePage.js
 * Author: Jean-Francois Desgagne (jfdesgagne@gmail.com)
 * Date: 09/27/2013
 *
 * MoviePage is the 2nd navigation level in the app. This page is prompt
 * when a thumbnail is tapped, and destroyed when the Cancel button is clicked.
 * It's a "modal", so its come with a nice CSS3 transition from the bottom.
 *
 */

Netflix.MoviePage = function(id) {
	Netflix.BaseView.call(this, "MoviePage", true);
	///Private members
	var thumbnail	= null,
		title		= new Netflix.Sprite("", "", "h1"),
		addButton 	= new Netflix.Sprite("", "addButton");

	//Setup the header
	this.header.addButtons("cancel");
	this.header.addEventListener(Netflix.Header.BUTTON_TAPPED_EVENT, this.onCancel, this);

	//Creating & adding to the view the Thumbnail, the title and the "Instant Queue" button
	thumbnail = new Netflix.Thumbnail(Netflix.MovieListService.getInstance().getThumbnail(id), id, false, true),
	this.content.appendChild(thumbnail);
	title.view.innerHTML = Netflix.MovieListService.getInstance().getMovieTitle(id);
	this.content.appendChild(title);
	addButton.view.innerHTML = "Instant Queue";
	this.content.appendChild(addButton);
	
};
Netflix.MoviePage.prototype = new Netflix.BaseView();
Netflix.MoviePage.prototype.onCancel = function(button) {
	if(button == "cancel") {
		this.dispatch(Netflix.CANCEL_EVENT);
	}
};