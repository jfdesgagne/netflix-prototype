/**
 * 
 * HomePage.js
 * Author: Jean-Francois Desgagne (jfdesgagne@gmail.com)
 * Date: 09/26/2013
 *
 * The HomePage is the first view in the app. It contains
 * a bunch of list of movies (horizontal list).
 * When the page is scroll vertically, it will ask each list to refresh itself.
 * There is no caching mechanism (ran out of time), but ideally something like 
 * I did in MovieList could be an option.
 *
 */

Netflix.HomePage = function() {
	Netflix.BaseView.call(this, "HomePage");
	
	var that 	= this,
		movies 	= null;
		
	this.moviesList = [];

	//is not part of the prototype, since we want to make sure the scoping is this class 
	//(and not the DOM element by example if we do element.addEventListener())
	this.onScroll = function() {
		that.updateThumbnails();
	};

	//Setup the header
	this.header.addButtons("options", "search");

	//Populate a list of movies row
	movies = Netflix.MovieListService.getInstance().getMovies();
	for(var i=0; i<movies.length; i++) {
		var movieList = new Netflix.MovieList(movies[i].summary.displayName, movies[i].movies);
		movieList.addEventListener(Netflix.MOVIE_SELECTED_EVENT, this.onMovieSelected, this);
		this.moviesList.push(movieList);
		this.content.appendChild(movieList);
	}

	//When the page scroll, check for any news thumbnails on screen
	this.content.view.addEventListener("scroll", this.onScroll);
};

Netflix.HomePage.prototype = new Netflix.BaseView();
Netflix.HomePage.prototype.onMovieSelected = function(movieId) {
	this.dispatch(Netflix.MOVIE_SELECTED_EVENT, movieId);
};
Netflix.HomePage.prototype.destroy = function() {
	this.content.view.removeEventListener("scroll", this.onScroll);
	delete this.moviesList;
	delete this.onScroll;

	Netflix.BaseView.prototype.destroy.apply(this, arguments);
};
Netflix.HomePage.prototype.updateThumbnails = function() {
	for(var i=0; i<this.moviesList.length; i++) {
		this.moviesList[i].updateThumbnails();
	}
};