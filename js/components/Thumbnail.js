/**
 * 
 * Thumbnail.js
 * Author: Jean-Francois Desgagne (jfdesgagne@gmail.com)
 * Date: 09/26/2013
 *
 * Thumbnail (used in HomePage and MoviePage)
 * You can set the 3rd arguments to true if you want to have the overlay
 * visible only when user long press a thumbnail (used in HomePage).
 * Otherwise it will always be visible by default (like in MoviePage)
 *
 */

Netflix.Thumbnail = function(src, id, longPressOverlay, forceLoading) {
	Netflix.Sprite.call(this, "", "thumbnail");

	var that = this;

	this.image 				= null;
	this.imageSrc 			= src;
	this.overlay 			= null;
	this.id					= id;
	this.ignoreTap			= false;

	//To avoid the click event and the LONGPRESS_END_EVENT event to both trigger the onTap()
	this.onTap				= function(event) {
		if(event && that.ignoreTap) return;
		that.dispatch(Netflix.MOVIE_SELECTED_EVENT, that.id);
	};
	this.onTouchStart		= function(event) {
		that.ignoreTap = false;
	};


	//Creating the thumbnail image
	this.image = new Netflix.Sprite(null, null, "img");
	if(forceLoading === true) this.image.view.src = this.imageSrc;
	this.appendChild(this.image);

	//If the longPressOverlay is set, we will listen for the LONGPRESS_START_EVENT event.
	//Otherwise, we will always show the overlay
	if(longPressOverlay) {
		this.addEventListener(Netflix.Sprite.LONGPRESS_START_EVENT, this.onLongPressStart, this);
	} else {
		this.showOverlay(true);
	}

	this.view.addEventListener("click", this.onTap);
	this.view.addEventListener("touchstart", this.onTouchStart);

};
Netflix.Thumbnail.prototype = new Netflix.Sprite();
Netflix.Thumbnail.prototype.destroy = function() {
	this.view.removeEventListener("click", this.onTap);
	this.view.removeEventListener("touchstart", this.onTouchStart);

	delete this.image;
	delete this.imageSrc;
	delete this.overlay;
	delete this.id;
	delete this.ignoreTap;
	delete this.onTa;
	delete this.onTouchStart;

	Netflix.Sprite.prototype.destroy.apply(this, arguments);
};
Netflix.Thumbnail.prototype.updateVisibility = function(event) {
	this.ignoreTap = false;
};
Netflix.Thumbnail.prototype.load = function(event) {
	if(!this.isLoaded()) {
		this.image.view.src = this.imageSrc;
	}
};
Netflix.Thumbnail.prototype.isLoaded = function(event) {
	return this.image.view.src.length > 0;
};
Netflix.Thumbnail.prototype.showOverlay = function(val) {
	if(val == undefined) val = false;
	if(val) {
		this.overlay = new Netflix.Sprite(null, "overlay");
		this.appendChild(this.overlay);
	} else {
		if(this.overlay != null) {
			this.overlay.destroy();
			this.overlay = null;
		}
	}
};
Netflix.Thumbnail.prototype.onLongPressStart = function() {
	this.showOverlay(true);
	this.addEventListener(Netflix.Sprite.LONGPRESS_END_EVENT, this.onLongPressEnd, this);
};
Netflix.Thumbnail.prototype.onLongPressEnd = function(thumnail, cancelled) {
	this.showOverlay(false);
	this.removeEventListener(Netflix.Sprite.LONGPRESS_END_EVENT, this.onLongPressEnd, this);
	this.ignoreTap = true;
	if(cancelled !== true) {
		this.onTap();
	}	
};