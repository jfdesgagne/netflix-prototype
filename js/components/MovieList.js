/**
 * 
 * MovieList.js
 * Author: Jean-Francois Desgagne (jfdesgagne@gmail.com)
 * Date: 09/26/2013
 *
 * The MovieList contains an horizontal list of Thumbnails (used in HomePage)
 * It handle the actions on each thumbnails, as well as the asynchronous load
 * when the list is scrolled.
 * 
 */

Netflix.MovieList = function(text, items) {
	Netflix.Sprite.call(this, "", "movieList");

	var that		= this,
		title		= null
		thumbnail 	= null;

	this.scroller 			= null;
	this.thumbnails 		= [];
	this.unloadedThumbnails = [];
	this.visiblePadding		= 600; //in pixels
	this.onScroll			= function() {
		that.updateThumbnails();
	};


	//Creating the title and scroller container
	title = new Netflix.Sprite("", "title");
	title.view.innerHTML = text; //I use innerHTML when there is no DOM changes, like for setting text
	this.appendChild(title);
	this.scroller = new Netflix.Sprite("", "scroller");
	this.appendChild(this.scroller);

	//Populating the thumbnails
	if(!Array.isEmpty(items)) {
		for(var i=0; i<items.length; i++) {
			thumbnail = new Netflix.Thumbnail(Netflix.MovieListService.getInstance().getThumbnail(items[i]), items[i], true);
			thumbnail.addEventListener(Netflix.MOVIE_SELECTED_EVENT, this.onMovieSelected, this);
			this.scroller.appendChild(thumbnail);
			this.thumbnails.push(thumbnail);
			this.unloadedThumbnails.push(thumbnail);
		}
	}

	this.scroller.view.addEventListener("scroll", this.onScroll);
};
Netflix.MovieList.prototype = new Netflix.Sprite();
Netflix.MovieList.prototype.onMovieSelected = function(movieId) {
	this.dispatch(Netflix.MOVIE_SELECTED_EVENT, movieId)
};
Netflix.MovieList.prototype.updateThumbnails = function() {
	for(var i=this.unloadedThumbnails.length-1; i>=0; i--) {
		if(this.unloadedThumbnails[i].isVisibleInViewport(this.visiblePadding)) {
			this.unloadedThumbnails[i].load();
			this.unloadedThumbnails.splice(i, 1);
		}
	}
}
Netflix.MovieList.prototype.destroy = function() {
	this.scroller.view.removeEventListener("scroll", this.onScroll);

	delete this.unloadedThumbnails;
	delete this.thumbnails;

	Netflix.Sprite.prototype.destroy.apply(this, arguments);
};