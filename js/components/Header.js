/**
 * 
 * Header.js
 * Author: Jean-Francois Desgagne (jfdesgagne@gmail.com)
 * Date: 09/27/2013
 *
 * The header is used by both page (HomePage & MoviePage)
 * It handle a set of button (passed either as argument in the constructor
 * or using the addButtons method).
 *
 */

Netflix.Header = function() {
	Netflix.Sprite.call(this, "", "header");

	var that = this;
		logo = null;

	this.buttons 			= [];
	this.buttonsList 		= [];
	this.onButtonTapped		= function(event) {
		for(var i=0; i<that.buttonsList.length; i++) {
			if(that.buttons[i].view == event.target) {
				that.dispatch(Netflix.Header.BUTTON_TAPPED_EVENT, that.buttonsList[i]); //send the text not the button
				return;
			}
		}	
	};


	//Creating the Netflix logo
	logo = new Netflix.Sprite("", "logo");
	this.appendChild(logo);

	//Add the header buttons from the constructor (could always use Netflix.Header.addButtons() later)
	if(!Array.isEmpty(arguments)) {
		this.addButtons(Array.prototype.slice.call(arguments));
	}
};
Netflix.Header.prototype = new Netflix.Sprite();
Netflix.Header.prototype.addButtons = function() {
	var button = null;
	var buttonsList = Array.prototype.slice.call(arguments);
	this.buttonsList = buttonsList.concat(buttonsList);

	if(!Array.isEmpty(buttonsList)) {
		for(var i=0; i<buttonsList.length; i++) {
			button = new Netflix.Sprite("", "button " + buttonsList[i]);
			button.view.addEventListener("click", this.onButtonTapped);
			this.appendChild(button);
			this.buttons.push(button);
		}
	}
};
Netflix.Header.prototype.destroy = function() {
	if(!Array.isEmpty(this.buttons)) {
		for(var i=0; i<this.buttons.length; i++) {
			this.buttons[i].view.removeEventListener("click", this.onButtonTapped);
			this.buttons[i].destroy();
		}
	}

	delete this.buttons;
	delete this.buttonsList;

	Netflix.Sprite.prototype.destroy.apply(this, arguments);
};

//Events
Netflix.Header.BUTTON_TAPPED_EVENT = "Netflix.Header.BUTTON_TAPPED_EVENT";