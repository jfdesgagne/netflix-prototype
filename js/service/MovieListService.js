/**
 *
 * MovieListService.js
 * Author: Jean-Francois Desgagne (jfdesgagne@gmail.com)
 * Date: 09/26/2013
 *
 * This is the main service for the app. I do not have a
 * data model, since the data given for this demo
 * is already a model itself.
 * I also do not load asynchronously from a remote server.
 * Normally, it should, but I don't have the time unfortunally.
 *
 */

Netflix.MovieListService = function() {
	Netflix.EventDispatcher.apply(this, arguments);
};
Netflix.MovieListService.prototype = new Netflix.EventDispatcher();

//Retrieve the data (locally)
Netflix.MovieListService.prototype.load = function() {
	this.rawData = Netflix.MOVIE_LIST_DATA;
};

Netflix.MovieListService.prototype.getThumbnail = function(id) {
	return this.rawData.movies[id]["summary"]["box_art"]["284x405"];
};

Netflix.MovieListService.prototype.getMovies = function() {
	return this.rawData.lists;
};

Netflix.MovieListService.prototype.getMovieTitle = function(id) {
	return this.rawData.movies[id]["summary"]["title"]["title_short"];
};