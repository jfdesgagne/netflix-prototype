/**
 * 
 * Netflix.js
 * Author: Jean-Francois Desgagne (jfdesgagne@gmail.com)
 * Date: 09/26/2013
 *
 * Main file for prototype of Netflix mobile app.
 * This demo is made compatible only for webkit browsers
 * and was only tested on an iPhone 4 with iOS 6.
 * There are 2 main pages (HomePage.js and MoviePage.js).
 * This class handle the interaction between them.
 *
 */

Netflix.Netflix = function() {
	Netflix.Sprite.call(this, "Netflix");
	this.homePage 	= null;
	this.moviePage 	= null;

	//add the main view at the root of the document
	document.body.appendChild(this.view);

	//normally, this would be asynchronous, but for this demo, lets just load from mockResponse.js
	Netflix.MovieListService.getInstance().load();

	//Creating the default homepage
	this.homePage = new Netflix.HomePage();
	this.appendChild(this.homePage);
	this.homePage.updateThumbnails(); //call updatethumbnails once to load the thumbnails that are on curently on screen
	this.homePage.addEventListener(Netflix.MOVIE_SELECTED_EVENT, this.onMovieSelected, this);

};
Netflix.Netflix.prototype = new Netflix.Sprite();
Netflix.Netflix.prototype.onMovieSelected = function(movieId) {
	if(this.moviePage == null) {
		this.moviePage = new Netflix.MoviePage(movieId);
		this.appendChild(this.moviePage);
		this.moviePage.addEventListener(Netflix.CANCEL_EVENT, this.onMoviePageClosed, this);
		this.moviePage.show();
	}
};
Netflix.Netflix.prototype.onMoviePageClosed = function() {
	this.moviePage.removeEventListener(Netflix.CANCEL_EVENT, this.onMoviePageClosed, this);
	this.moviePage.show(false);
	this.moviePage = null;
};