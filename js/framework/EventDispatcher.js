/**
 *
 * EventDispatcher.js
 * Author: Jean-Francois Desgagne (jfdesgagne@gmail.com)
 * Date: 09/26/2013
 *
 * EventDispatcher is the base of every smart object in this demo.
 * It add the ability to add/remove or check if an object has some
 * events attached to it. This is similar to ActionScript 3 
 * implementation of events.
 *
 */


Netflix.EventDispatcher = function() {
    this.eventListeners = {};
};
Netflix.EventDispatcher.prototype.destroy = function() {
    this.eventListeners = null;
};
Netflix.EventDispatcher.prototype.addEventListener = function(type, callback, scope) {
    scope = scope || this;
    if(!Array.isEmpty(this.eventListeners[type])) {
        this.eventListeners[type].push({scope:scope, callback:callback});
    } else {
        this.eventListeners[type] = [{scope:scope, callback:callback}];
    }
};
Netflix.EventDispatcher.prototype.removeEventListener = function(type, callback, scope) {
    scope = scope || this;
    if(!Array.isEmpty(this.eventListeners[type])) {
        var len = this.eventListeners[type].length;
        for(var i=len-1; i>=0; i--) {
            var eventListener = this.eventListeners[type][i];
            if(eventListener.scope == scope && eventListener.callback == callback) {
                this.eventListeners[type].splice(i, 1);
            }
        }
        if(this.eventListeners[type].length == 0) {
            delete this.eventListeners[type];
        }
    }
};
Netflix.EventDispatcher.prototype.hasEventListener = function(type, callback, scope) {
    scope = scope || this;
    if(!Array.isEmpty(this.eventListeners[type])) {
        if(arguments.length == 1) return this.eventListeners[type].length > 0;
        for(var i=0; i<this.eventListeners[type].length; i++) {
            var eventListener = this.eventListeners[type][i];
            if(scope == eventListener.scope && eventListener.callback == callback) {
                return true;
            }
        }
    }
};
Netflix.EventDispatcher.prototype.dispatch = function(type) {
    var args = arguments.length > 1 ? Array.prototype.slice.call(arguments, 1) : [];

    if(!Array.isEmpty(this.eventListeners[type])) {
        var len = this.eventListeners[type].length;
        for(var i=len-1; i>=0; i--) {
            var eventListener = this.eventListeners[type][i];
            eventListener.callback.apply(eventListener.scope, args);
        }
    }
};