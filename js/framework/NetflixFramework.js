/**
 * 
 * NetflixFramework.js
 * Author: Jean-Francois Desgagne (jfdesgagne@gmail.com)
 * Date: 09/26/2013

 * Declaration of the Netflix namespace, as well as generic events names.
 *
 * The class herarchie is the following:
 *      Netflix.EventDispatcher
 *      --> Netflix.Sprite
 *          --> Netflix.BaseView
 *
 * There are also a Singleton pattern FunctionName.getInstance(), that keeps an internal reference of the constructed function, until FunctionName.destroyInstance() is called
 *
 */

//Singleton util, encapsulated in his own scope to avoid dirtying the global scope
(function() {
    var singletonInstances = {};
    Function.prototype.getInstance = function() {
        if(singletonInstances[this] == undefined || singletonInstances[this] == null) {
            singletonInstances[this] = new this();
        }
        return singletonInstances[this];
    };

    //in case the singleton isn't wanted anymore
    Function.prototype.destroyInstance = function() {
        if(singletonInstances[this] != undefined && singletonInstances[this] != null) {
            delete singletonInstances[this];
        }
    };
}());

//Those are intended to be static methods, easier easier for readibility
Array.isEmpty = function(val) {return (val != undefined && val && val instanceof Array && val.length > 0) == false;};


//create a namespace for the netflix demo
var Netflix = {}; 

//Generic events
Netflix.MOVIE_SELECTED_EVENT = "Netflix.MOVIE_SELECTED_EVENT";
Netflix.CANCEL_EVENT = "Netflix.CANCEL_EVENT";
