/**
 * 
 * Sprite.js
 * Author: Jean-Francois Desgagne (jfdesgagne@gmail.com)
 * Date: 09/26/2013
 *
 * Base class for DOM Element object. 
 * Support LongPress events.
 * Inteligent recursive destructor for added Sprite 
 * childs with Sprite.addChild method.
 * If one argument is passed, the view will be initiated. Otheriwse,
 * you need to call Sprite.initView yourself.
 *
 */

Netflix.Sprite = function(id, className, tagName) {
	Netflix.EventDispatcher.apply(this, arguments);

    this.view               = null;
    this.children           = [];

    var tapInitPos          = null,
        longpressTimeout    = 0,
        that                = this,
        onWindowTouchMove = function(event) {
            if(
                event.touches[0].screenX > tapInitPos.x + Netflix.Sprite.TAP_THRESHOLD ||
                event.touches[0].screenX < tapInitPos.x - Netflix.Sprite.TAP_THRESHOLD ||
                event.touches[0].screenY > tapInitPos.y + Netflix.Sprite.TAP_THRESHOLD ||
                event.touches[0].screenY < tapInitPos.y - Netflix.Sprite.TAP_THRESHOLD) 
            {
                cancelTouch(true);        
            }
        },
        cancelTouch = function(cancelled) {
            tapStarted = false;
            if(longpressTimeout > 0) {
                clearInterval(longpressTimeout);
                longpressTimeout = 0;
            }
            tapInitPos = null;
            window.removeEventListener("touchmove", onWindowTouchMove);
            that.view.removeEventListener("touchleave", cancelTouch);
            that.view.removeEventListener("touchend", cancelTouch);
            
            that.dispatch(Netflix.Sprite.LONGPRESS_END_EVENT, that, cancelled===true);
        },
        onLongPressTimeout = function() {
            that.dispatch(Netflix.Sprite.LONGPRESS_START_EVENT, that);
        },
        onTouchStart = function(event) {
            tapInitPos = {x:event.touches[0].screenX, y:event.touches[0].screenY};
            longpressTimeout = setTimeout(onLongPressTimeout, Netflix.Sprite.LONGPRESS_DELAY);

            window.addEventListener("touchmove", onWindowTouchMove);
            that.view.addEventListener("touchleave", cancelTouch);
            that.view.addEventListener("touchend", cancelTouch);
        };


    this.addEventListener = function(type, callback, scope) {
        if(!this.hasTouchEventListner() && (type == Netflix.Sprite.LONGPRESS_START_EVENT || type == Netflix.Sprite.LONGPRESS_END_EVENT)) {
           this.view.addEventListener("touchstart", onTouchStart);
        }
        Netflix.EventDispatcher.prototype.addEventListener.call(this, type, callback, scope);
    }

    this.removeEventListener = function(type, callback, scope) {
    	Netflix.EventDispatcher.prototype.removeEventListener.call(this, type, callback, scope);
        if(!this.hasTouchEventListner()) {
            this.view.removeEventListener("touchstart", onTouchStart);
        }
    }

    if(arguments.length > 0) {
    	this.initView(id, className, tagName);
    }
};
Netflix.Sprite.prototype = new Netflix.EventDispatcher();
Netflix.Sprite.prototype.initView = function(id, className, tagName) {
	if(this.view == null) {
        this.view = document.createElement(tagName || "div");
    }

    if(typeof id === 'string') this.view.id = id;
    if(typeof className === 'string') this.view.className = className;
};
Netflix.Sprite.prototype.hasTouchEventListner = function() {
    return this.hasEventListener(Netflix.Sprite.LONGPRESS_END_EVENT) || this.hasEventListener(Netflix.Sprite.LONGPRESS_START_EVENT);
};

//The appendChild and removeChild are utility function, but are also useful for auto-destroy
Netflix.Sprite.prototype.appendChild = function(child) {
    if(child instanceof Netflix.Sprite) {
        this.children.push(child);
        this.view.appendChild(child.view);
    } else {
        this.view.appendChild(child);
    }
};
Netflix.Sprite.prototype.removeChild = function(child) {
    if(child instanceof Netflix.Sprite) {
        this.children.splice(this.children.indexOf(child), 1);
        this.view.appendChild(child.view);
    } else {
        this.view.appendChild(child);
    }
};
Netflix.Sprite.prototype.destroy = function() {
    //find every children and destroy them
    for(var i=0; i<this.children.length; i++) {
        this.children[i].destroy();
    }
    this.children = [];

    //If his view is attached to a parent, remove it
    if(this.view.parentNode) {
        this.view.parentNode.removeChild(this.view);
    }

    //This will free all his listeners
    Netflix.EventDispatcher.prototype.destroy.apply(this, arguments);
};
Netflix.Sprite.prototype.show = function(val) {
    if(val == undefined) val = true;
    if(val) {
        this.forceRefreshStyles();
        this.view.setAttribute("show", true);
    } else {
        this.view.removeAttribute("show");
    }
};
Netflix.Sprite.prototype.isVisibleInViewport = function(padding) {
    if(padding == undefined) padding = 0;
    var rect = this.view.getBoundingClientRect();

    return (
        rect.width > 0 &&
        rect.height > 0 &&
        rect.bottom >= -padding &&
        rect.right >= -padding &&
        rect.top <= (window.innerHeight || document.body.clientHeight) + padding &&
        rect.left <= (window.innerWidth || document.body.clientWidth) + padding
    );
};

//Little hack to force the browser to refresh the styles (useful when the element has just been added and we want to do an animation right away)
Netflix.Sprite.prototype.forceRefreshStyles = function(val) {
     window.getComputedStyle(this.view).getPropertyValue("top");
};

//static const
Netflix.Sprite.LONGPRESS_DELAY = 300;
Netflix.Sprite.TAP_THRESHOLD = 20;

//events
Netflix.Sprite.LONGPRESS_START_EVENT = "Netflix.Sprite.LONGPRESS_START_EVENT";
Netflix.Sprite.LONGPRESS_END_EVENT = "Netflix.Sprite.LONGPRESS_END_EVENT";