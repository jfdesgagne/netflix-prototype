/**
 *
 * BaseView.js
 * Author: Jean-Francois Desgagne (jfdesgagne@gmail.com)
 * Date: 09/26/2013
 *
 * The BaseView class constain an Header and a content.
 * The header is static at the top, while the content is
 * vertically scrollable. Only HomePage and MoviePage extends it.
 *
 */

Netflix.BaseView = function(id, isModal) {
	Netflix.Sprite.call(this, id, "view");

	var that = this;

    this.header     = new Netflix.Header();
    this.content    = new Netflix.Sprite("", "viewContent");
    this.isModal    = isModal;


    //Modal view have opening transitions
    if(isModal) {
        that.view.setAttribute("modal", true);
    }
    that.appendChild(that.header);
    that.appendChild(that.content);
};
Netflix.BaseView.prototype = new Netflix.Sprite();
Netflix.BaseView.prototype.show = function(val) {
    if(!val && this.isModal && this.view.hasAttribute("show")) {
    	var that = this;
    	var onWebkitTransitionEnd = function() {
	    	that.view.removeEventListener('webkitTransitionEnd', onWebkitTransitionEnd);
	        that.destroy();
	    }

        this.view.addEventListener('webkitTransitionEnd', onWebkitTransitionEnd);
      
    }
    Netflix.Sprite.prototype.show.apply(this, arguments);
};